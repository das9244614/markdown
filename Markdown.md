# Documentacion con Doxygen para Java

## Prerrequisitos

1. Instalar Doxygen

Para instalar doxygen podemos entrar a la página oficial:
https://www.doxygen.nl/download.html

- Descargamos el .exe e instalamos

![Descarga](Descarga.png)

2. Tener instalado nuestro IDE de preferencia en mi caso:

```shell script
IntellijIDEA
```

## En nuestro codigo

1. Generar comentarios en el codigo con la estructura correcta para Java

- Ejemplo en script:

```shell script
package septimo.documentacion7a;

/**
 * Clase Cliente representa a un cliente en el sistema.
 * Almacena informacion básica del cliente como nombre, apellido, género y edad.
 * @author MateoM
 * @version 7.0
 * @since 1.0
 */

public class Cliente {
    String nombre;
    String apellido;
    char genero;
    int edad;
}
```
- Ejemplo en el IDE:

![Comentarios](Comentarios.png)

2. Generamos el javadoc de nuestro proyecto con la ayuda de nuestro IDE

- En mi caso tuve que ir a Tools -> Generate Javadoc -> Configurar las opciones

![GenerateJavaDoc](JavaDoc.png)

- Al abrir el Javadoc generado se ve de la siguiente manera:

![JavaDocGenerado](JavaDocGenerado.png)

## Pasos a seguir en Doxygen:

1. Abrimos la aplicación de Doxygen y configuramos

1.1. Nos encontramos con la pantalla principal y debemos configurar 3 rutas:

- Ruta donde se va a ejecutar el Doxygen

![Ruta1](DoxSelect1.png)

- Ruta del paquete donde estan las clases de nuestro proyecto

![Ruta2](DoxSelect2.png)

- Ruta de destino donde se van a guardar los archivos generados de Doxygen

![Ruta3](DoxSelect3.png)

Una vez configurado se vera el apartado Principal de la siguiente manera:

![Principal](DoxPrincipal.png)

1.2. La siguiente opcion a configurar es Mode y quedará de la siguiente manera:

![Mode](Mode.png)

1.3. La siguiente opcion a configurar es Output y quedará de la siguiente manera:

![Output](Output.png)

1.4. La siguiente opcion a configurar es Diagrams y la dejaremos como viene por defecto

2. Le damos next hasta llegar a esta opción

- Le damos a Run Doxygen:

![RunDox](RunDox.png)

- Una vez terminado el proceso, los logs deberan verse de la siguiente manera:

![DoxLogs](DoxLogs.png)

## Para ver el resultado de Doxygen

- Abrimos la ruta de destino de Doxygen escogida

- Nos dirigimos al archivo 

```shell script
index.html
```

- Lo abrimos y podemos visualizar la documentacion de Doxygen

![Resultado](Resultado.png)
